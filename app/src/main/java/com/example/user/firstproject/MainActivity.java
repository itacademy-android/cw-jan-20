package com.example.user.firstproject;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button button1, button2, button3;
    TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Предоставление классу layout файл.
//        setContentView(R.layout.myscreen);
        setContentView(R.layout.activity_main);

        //привязывем view-компоненты
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);

        textView = findViewById(R.id.textView);


        // Первый вариант установки обработчика
        /*button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button1.setText("clicked!");
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button2.setText("clicked 2!");
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button3.setText("clicked 3!");
            }
        });*/

        //ставим обраотчик на кнопки
        button1.setOnClickListener(onClickListener);
        button2.setOnClickListener(onClickListener);
        button3.setOnClickListener(onClickListener);

    }

    //второй вариант установки обработчика
    //Не забываем указать в xml файле во view-компоненте Button опцию anroid:onClick="onClick"
    /*public void onClick(View view) {
        button1.setText("clicked!");
    }*/

    // третий вариант установки обработчика
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.button1:
                    button1.setText("clicked!");
                    break;
                case R.id.button2:
                    button2.setText("clicked 2!");
                    break;
                case R.id.button3:
                    textView.setText("clicked 3!");
                    textView.setTextColor(getResources().getColor(R.color.colorRed));
                    break;
            }
        }
    };


}
