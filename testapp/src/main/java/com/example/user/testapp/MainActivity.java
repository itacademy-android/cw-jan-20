package com.example.user.testapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    CheckBox checkBox;
    Button btnSayHello;
    EditText etName;
    TextView tvGreeter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Связываем view-компонетны с класом
        checkBox = findViewById(R.id.checkboxToast);
        btnSayHello = findViewById(R.id.btnSayHello);
        etName = findViewById(R.id.etName);
        tvGreeter = findViewById(R.id.tvGreeter);

        //инициализация обработчиков
        btnSayHello.setOnClickListener(onClickListener);
        checkBox.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    //Обработчик для чекбокса
    CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            if (isChecked) {
                btnSayHello.setEnabled(false);
            } else {
                btnSayHello.setEnabled(true);
            }
        }
    };

    //Обработчик для кнопки
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            tvGreeter.setText("Hello " + etName.getText().toString());
        }
    };

    //Создание простого меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //вызываем службу создания меню
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Метод для обработки событий на простом меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_hide:
                checkBox.setVisibility(View.INVISIBLE);
                break;
            case R.id.action_extra:
                Log.d("TAG", "CLICKED");
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
